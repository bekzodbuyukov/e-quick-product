from django.urls import path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from product.views import (ProductViewSet, ProductCategoryGroupViewSet,
                           ProductsByCategory, ProductsByCompany,)


router = routers.SimpleRouter()
router.register('product', ProductViewSet)
router.register('product_category_group', ProductCategoryGroupViewSet)


urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token-obtain'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
    path('products_by_category/<int:category_id>/', ProductsByCategory.as_view(), name='products_by_category'),
    path('products_by_company/<int:company_id>/', ProductsByCompany.as_view(), name='products_by_company'),
]

urlpatterns += router.urls
