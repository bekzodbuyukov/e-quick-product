import os
import environ


env = environ.Env(DEBUG=(bool, False))

# Set project base directory (/product)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Read environment variables from .env file (/product/.env)
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))

# Env variables
DEBUG = env('DEBUG')
SECRET_KEY = env('SECRET_KEY')
DB_NAME = env('DB_NAME')
DB_USER = env('DB_USER')
DB_PASS = env('DB_PASS')
DB_HOST = env('DB_HOST')
DB_PORT = env('DB_PORT')
