import requests
from django.db import models


class ProductCategoryGroup(models.Model):
    """ Model which defines the Category of Product """
    product = models.ForeignKey('Product', on_delete=models.CASCADE, verbose_name='Product')
    category_id = models.IntegerField(default=1, verbose_name='ID of Category')

    def __str__(self):
        """ Method is used to display an object in Django admin """
        return f'Object-ID: {self.id} / {self.product} / Category-ID: {self.category_id}'

    class Meta:
        """ Meta Class """
        verbose_name = 'Product Category Group'
        verbose_name_plural = 'Product Category Groups'


class Product(models.Model):
    """ Model which defines Product in a system """
    sap_id = models.IntegerField(default=0, verbose_name='SAP ID', null=True, blank=True)
    savdo_id = models.IntegerField(default=0, verbose_name='Online Savdo ID', null=True, blank=True)
    company_id = models.IntegerField(default=1, blank=False, null=False, verbose_name='Company ID')

    name = models.CharField(max_length=255, verbose_name='Name of Product')
    description = models.TextField(blank=True, null=True, verbose_name='Description of Product')
    picture = models.ImageField(default='/media/defaults/product.png', blank=True,
                                null=True, verbose_name='Image of Product')
    price = models.PositiveBigIntegerField(default=0, verbose_name='Price of Product')
    amount = models.IntegerField(default=0, verbose_name='Amount of Product')
    options = models.JSONField(blank=True, null=True, verbose_name='Options of Product')

    STATUS_TYPES = {
        0: 'disactive',
        1: 'active'
    }
    status = models.IntegerField(default=STATUS_TYPES[0], verbose_name='Status of Product')

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Product added time')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Product updated time')

    def fill_from_file(self, json_data):
        """ Method to fill Object Instance from static JSON data """
        self.sap_id = json_data['sap_id'] if json_data['sap_id'] else 0
        self.savdo_id = json_data['savdo_id'] if json_data['savdo_id'] else 0
        self.company_id = json_data['company_id'] if json_data['company_id'] else 0
        self.name = json_data['savdo_name'] if json_data['savdo_name'] else json_data['sap_name']
        self.description = json_data['options']['description']
        self.picture = '/media/defaults/product.png'
        self.price = json_data['price'] if json_data['price'] else 0
        self.amount = json_data['options']['amount'] if json_data['options']['amount'] else 0
        self.options = json_data['options']

        self.save()

    @classmethod
    def get_company_id(cls, company_unique_name):
        """ Method to get Company ID from Company service """
        url = f'https://domain/company/{company_unique_name}'
        response = requests.get(url).json()

        return response['company_id'] if response['company_id'] else 0

    @classmethod
    def get_category_id(cls, category_unique_name):
        """ Method to get Category ID from Category service """
        url = f'https://domain/category/{category_unique_name}'
        response = requests.get(url).json()

        return response['category_id'] if response['category_id'] else 0

    def __str__(self):
        """ Method is used to display an object in Django admin """
        return f'Object-ID: {self.id} Name: {self.name}'

    class Meta:
        """ Meta Class """
        verbose_name = 'Product'
        verbose_name_plural = 'Products'
