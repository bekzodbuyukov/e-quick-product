from rest_framework import serializers

from .models import Product, ProductCategoryGroup


class ProductCategoryGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategoryGroup
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
