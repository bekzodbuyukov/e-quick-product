from rest_framework import viewsets, generics, permissions, status
from rest_framework.response import Response

from .models import Product, ProductCategoryGroup
from .serializers import ProductSerializer, ProductCategoryGroupSerializer
from helpers import responses


class ProductCategoryGroupViewSet(viewsets.ModelViewSet):
    queryset = ProductCategoryGroup.objects.all()
    serializer_class = ProductCategoryGroupSerializer
    permission_classes = [permissions.BasePermission]


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.BasePermission]


# TODO: ProductsByCategory and ProductsByCompany very similar.
#  So, make them something one general (make general Manager or etc.).
class ProductsByCategory(generics.ListAPIView):
    queryset = ProductCategoryGroup.objects.all()
    permission_classes = [permissions.BasePermission]

    def get(self, request, *args, **kwargs):
        category_id = kwargs['category_id']

        try:
            category_groups = self.queryset.filter(category_id=category_id).all()
        except:
            return Response(data=responses.error['products_by_category'], status=status.HTTP_400_BAD_REQUEST)
        else:
            if len(category_groups) == 0:
                return Response(data=responses.error['products_by_category'], status=status.HTTP_400_BAD_REQUEST)

        response_data = [
            {
                'category_id': category_id,
                'total_products_found': len(category_groups),
            },
            {
                'products': [],
            }
        ]
        for category_group in category_groups:
            product = category_group.product
            # TODO: Can use Serializer for serializing data, instead of doing it myself
            response_data[1]['products'].append({
                "id": product.id,
                "sap_id": product.sap_id,
                "savdo_id": product.savdo_id,
                "company_id": product.company_id,
                "name": product.name,
                "description": product.description,
                "picture": product.picture.url,
                "price": product.price,
                "amount": product.amount,
                "options": product.options,
                "status": product.status,
                "created_at": product.created_at,
                "updated_at": product.updated_at
            })

        return Response(data=response_data, status=status.HTTP_200_OK)


class ProductsByCompany(generics.ListAPIView):
    queryset = Product.objects.all()
    permission_classes = [permissions.BasePermission]

    def get(self, request, *args, **kwargs):
        company_id = kwargs['company_id']

        try:
            products_by_company = self.queryset.filter(company_id=company_id).all()
            print(products_by_company)
        except:
            return Response(data=responses.error['products_by_company'], status=status.HTTP_400_BAD_REQUEST)
        else:
            if len(products_by_company) == 0:
                return Response(data=responses.error['products_by_company'], status=status.HTTP_400_BAD_REQUEST)

        response_data = [{
            'company_id': company_id,
            'total_products_found': len(products_by_company),
        }, {
            'products': []
        }]
        for product in products_by_company:
            response_data[1]['products'].append({
                "id": product.id,
                "sap_id": product.sap_id,
                "savdo_id": product.savdo_id,
                "company_id": product.company_id,
                "name": product.name,
                "description": product.description,
                "picture": product.picture.url,
                "price": product.price,
                "amount": product.amount,
                "options": product.options,
                "status": product.status,
                "created_at": product.created_at,
                "updated_at": product.updated_at
            })

        return Response(data=response_data, status=status.HTTP_200_OK)
