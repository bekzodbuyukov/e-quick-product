import json
from .models import Product, ProductCategoryGroup


def store_data(file_name: str):
    file = open(file_name)
    data = json.load(file)

    for json_data in data:
        product = Product.objects.create()
        product.fill_from_file(json_data)

        category_group = ProductCategoryGroup.objects.create()
        category_group.product = product
        category_group.category_id = json_data['category_id']
        category_group.save()


if __name__ == '__main__':
    file_name = ''
    store_data(file_name)
