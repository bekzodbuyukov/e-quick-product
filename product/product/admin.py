from django.contrib import admin

from .models import Product, ProductCategoryGroup


admin.site.register([Product, ProductCategoryGroup])
