error = {
    'products_by_company': {
        'detail': 'There is no product by given Company ID',
        'code': 'company_id_not_found',
        'messages': {
            'message': 'Company ID does not exist or was deleted',
        }
    },
    'products_by_category': {
        'detail': 'There is no product by given Category ID',
        'code': 'category_id_not_found',
        'messages': {
            'message': 'Category ID does not exist or was deleted',
        }
    }
}
